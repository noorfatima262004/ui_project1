# Basic libraries to import for completing the whole work.
import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
from PyQt5 import uic
import os
from PyQt5.QtGui import QPixmap



class Mainwindow(QMainWindow):
    def __init__(self):
        super(Mainwindow, self).__init__()
        uic.loadUi("scrape1.ui", self)  # Here we imported the QT Designer file which we made as Python GUI FIle.

        # Command to make the backgroud of Window transparent.
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        self.pushButton.clicked.connect(self.Push_button)
    def resetValues(self):
        self.District_Box.setCurrentText("Select District")
        self.Paper_Box.setCurrentText("Select Stamp Paper Type")
        self.Tehsil_Box.setCurrentText("Select Tehsil")
        self.Deed_Box.setCurrentText("Select Deed Name")
        self.Agent_Name_Text.setPlaceholderText("Enter Name")
        self.Agent_Contact_text.setPlaceholderText("Enter contact")
        self.Agent_Cinc_text.setPlaceholderText("Enter CNIC")
        self.Agent_email.setPlaceholderText("Enter Email")
        self.PArtyNameText.setPlaceholderText("Enter Name")
        self.Party_Relation_Text.setPlaceholderText("Enter Relation")
        self.PartyCINCText.setPlaceholderText("Enter CNIC")
        self.PartyContactText.setPlaceholderText("Enter Contact")
        self.PartyMailText.setPlaceholderText("Enter Mail")
        self.Party_Addres.setPlaceholderText("Enter Address")

    def Push_button(self):
        challan = self.Challan_Details()
        Agent = self.Agent_Details()
        First_Party = self.First_Party_Details()


        Header = ['District', 'Paper', 'Tehsil', 'Deed', 'Agent Name', 'Contact', 'CNIC', 'Number','First Party Name' , 'First Party Relation','First Party CNIC','First Party Mail','First Party Contact','First Party Address']

        file_exists = os.path.isfile('student.csv')

        with open('student.csv', 'a+', encoding='utf-8', newline='') as fileInput:
            writer = csv.DictWriter(fileInput, fieldnames=Header)
            if not file_exists:
                writer.writeheader()
            writer.writerow({
                'District': challan[0],
                'Paper': challan[1],
                'Tehsil': challan[2],
                'Deed': challan[3],
                'Agent Name': Agent[0],
                'Contact': Agent[1],
                'CNIC': Agent[2],
                'Number': Agent[3],
                'First Party Name': First_Party[0],
                'First Party Relation': First_Party[1],
                'First Party CNIC': First_Party[2],
                'First Party Mail': First_Party[3],
                'First Party Contact': First_Party[4],
                'First Party Address': First_Party[5],
            })
        self.resetValues()

    def Challan_Details(self):
        selected_district = self.District_Box.currentText()
        selected_paper = self.Paper_Box.currentText()
        selected_teshsil = self.Tehsil_Box.currentText()
        selected_Deed = self.Deed_Box.currentText()

        data = [selected_district,selected_teshsil ,selected_paper, selected_Deed]
        return data

    def Agent_Details(self):
        name = self.Agent_Name_Text.text()
        contact = self.Agent_Contact_text.text()
        cnic = self.Agent_Cinc_text.text()
        number = self.Agent_email.text()
        data = [name, contact, cnic, number]
        return data

    def First_Party_Details(self):
        name = self.PArtyNameText.text()
        Realtion = self.Party_Relation_Text.text()
        Cinc = self.PartyCINCText.text()
        contact = self.PartyContactText.text()
        Mail = self.PartyMailText.text()
        Address = self.Party_Addres.text()

        data = [name, contact, Cinc, Mail,Realtion,Address]
        return data


app = QApplication(sys.argv)
window = Mainwindow()
window.show()
sys.exit(app.exec_())